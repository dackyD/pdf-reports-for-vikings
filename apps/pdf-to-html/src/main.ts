/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';
import * as bodyParser from 'body-parser';
import tmp from 'tmp';
import * as fs from 'fs';
import puppeteer from 'puppeteer';

const app = express();

app.use(bodyParser.json());

app.post('/', (req, res) => {
  tmp.file({
    prefix: 'html-to-pdf',
    postfix: '.html'
  }, 
  (err, path, fd, cleanupCallback) => {
    if (err) throw err;
    fs.write(fd, req.body.bodyHtml, async (fileErr, written, string) => {
      if (fileErr) throw fileErr;
      const browser = await puppeteer.launch();
      const page = await browser.newPage();
      await page.goto(`file://${path}`, { waitUntil: 'networkidle2'});
      const buffer = await page.pdf({
        format: 'A4',
        margin: {top: 40, bottom: 40},
        // headerTemplate: '',
        // footerTemplate: '',
        // displayHeaderFooter: true
      });
      await browser.close();
      res.contentType('application/pdf');
      res.send(buffer);
      cleanupCallback();
    });
  });
});

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/`);
});
server.on('error', console.error);
