import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationEditorComponent } from './application-editor/application-editor.component';
import { LoanPdfsModule } from '@pdf-reports-for-vikings/loan-pdfs';
import {initializer} from "./keycloak-initializer";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";

const appRoutes: Routes = [
  {
    path: 'applications',
    component: ApplicationListComponent
  },
  {
    path: 'applications/:id',
    component: ApplicationEditorComponent
  },
  {
    path: '',
    redirectTo: 'applications',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [AppComponent, ApplicationListComponent, ApplicationEditorComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { initialNavigation: 'enabled' }),
    HttpClientModule,
    LoanPdfsModule,
    KeycloakAngularModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
