import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { LoanApplication } from '@pdf-reports-for-vikings/loan-model';

@Component({
  selector: 'loan-application-editor',
  templateUrl: './application-editor.component.html',
  styleUrls: ['./application-editor.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationEditorComponent {

  application$: Observable<LoanApplication>;

  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient) {
    this.application$ = activatedRoute.paramMap.pipe (
      map(paramMap => paramMap.get('id')),
      switchMap(applicationId => http.get<LoanApplication>(`/loan-api/applications/${applicationId}`))
    );
  }

}
