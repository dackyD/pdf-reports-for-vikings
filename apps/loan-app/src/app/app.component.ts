import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'loan-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'loan-app';
  @HostBinding('class') class = 'p-8 bg-yellow-200';
}
