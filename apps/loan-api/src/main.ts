/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';
import cors from 'cors';
import fetch from 'node-fetch';
import * as Keycloak from 'keycloak-connect';
import { LoanApplication, LoanSubject, Status } from '@pdf-reports-for-vikings/loan-model'

const applications: LoanApplication[] = [
  {
    id: 1,
    status: Status.Approved,
    subject: LoanSubject.Boat,
    letterTexts: {
      greeting: "sdfjkfd",
    }
  }
]

const keycloak = new Keycloak({}, {
  "realm": "loan-application",
  "bearer-only": true,
  "auth-server-url": "http://localhost:42080/auth/",
  "ssl-required": "external",
  "resource": "report-api",
  "confidential-port": 0
});

const app = express();
app.use(cors());
app.use(keycloak.middleware());

app.get('/loan-api/applications', keycloak.protect(), (req, res) => {
  res.json(applications);
});

app.get('/loan-api/applications/:id', keycloak.protect(), (req, res) => {
  const applicationId = parseInt(req.params.id, 10);
  const application = applications.find(a => a.id === applicationId);
  res.json(application);
});

app.get('/loan-api/applications/:id/as-pdf', keycloak.protect(), async (req, res) => {
  const applicationId = parseInt(req.params.id, 10);
  const htmlResult = await fetch(`http://localhost:4400/pdf/application-decision/${applicationId}`, {
    headers: {
      'Authorization': `Bearer ${req.kauth.grant.access_token.token}`
    }
  });
  const reportAsHtml = await htmlResult.text();
  const fixedBase = reportAsHtml.replace('<base href="/">', '<base href="http://localhost:4400/">');
  const htmlToPdfRequest = {
    bodyHtml: fixedBase
  };

  const pdfResult = await fetch('http://localhost:3333/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(htmlToPdfRequest)
  });

  res.contentType('application/pdf');
  res.send(Buffer.from(await pdfResult.arrayBuffer()));
});

const port = process.env.port || 3334;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/loan-api`);
});
server.on('error', console.error);
