import { Component } from '@angular/core';

@Component({
  selector: 'pdf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'pdf-renderer';
}
