import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { LoanApplication } from '@pdf-reports-for-vikings/loan-model';

@Component({
  selector: 'pdf-decision-letter-renderer',
  templateUrl: './decision-letter-renderer.component.html',
  styleUrls: ['./decision-letter-renderer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DecisionLetterRendererComponent {

  application$: Observable<LoanApplication>;

  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient) {
    this.application$ = activatedRoute.paramMap.pipe (
      map(paramMap => paramMap.get('id')),
      switchMap(applicationId => http.get<LoanApplication>(`http://localhost:3334/loan-api/applications/${applicationId}`))
    );
  }

}
