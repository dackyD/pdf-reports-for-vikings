import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { LoanPdfsModule } from '@pdf-reports-for-vikings/loan-pdfs';
import { DecisionLetterRendererComponent } from './decision-letter-renderer/decision-letter-renderer.component';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  {
    path: 'pdf/application-decision/:id',
    component: DecisionLetterRendererComponent
  }
];

@NgModule({
  declarations: [AppComponent, DecisionLetterRendererComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { initialNavigation: 'enabled' }),
    LoanPdfsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
