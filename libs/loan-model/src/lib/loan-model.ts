export enum Status {
    Approved = 'APPROVED',
    Declined = 'DECLINED',
    Unknown = 'UNKNOWN'
}

export enum LoanSubject {
    Car = 'CAR',
    Boat = 'BOAT',
    Scooter = 'SCOOTER'
}

export interface LoanApplication {
    id: number;
    subject: LoanSubject;
    status: Status;
    letterTexts: object;
}
