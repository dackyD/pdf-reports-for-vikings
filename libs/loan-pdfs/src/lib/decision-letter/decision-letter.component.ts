import { Component, OnInit, ChangeDetectionStrategy, Input, HostBinding } from '@angular/core';
import { LoanApplication, Status } from '@pdf-reports-for-vikings/loan-model';

@Component({
  selector: 'loan-pdf-decision-letter',
  templateUrl: './decision-letter.component.html',
  styleUrls: ['./decision-letter.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DecisionLetterComponent {

  @HostBinding('class') class = 'p-10';

  @Input()
  model: LoanApplication;

  constructor() { }

  isApproved(): boolean {
    return this.model.status === Status.Approved;
  }

  isDeclined(): boolean {
    return this.model.status === Status.Declined;
  }


}
